use clap::Parser;

/// A software that compute the number of polycubes for a number of cubes
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    /// The number of cubes
    #[arg(short)]
    pub n: usize,

    /// The prefix of the paths of the cache files
    #[arg(long, default_value = "/tmp/polycubes_")]
    pub cache_prefix: String,
}
