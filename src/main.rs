#[cfg(not(target_os = "windows"))]
use tikv_jemallocator::Jemalloc;

#[cfg(not(target_os = "windows"))]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

use std::collections::{BTreeMap, BTreeSet};
use std::fs::OpenOptions;
use std::io::BufReader;
use std::sync::RwLock;
use std::{fmt, io::BufWriter};

use crate::cli::Args;
use clap::Parser;
use indicatif::{ParallelProgressIterator, ProgressStyle};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};

mod cli;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Axis {
    X,
    Y,
    Z,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
struct Point {
    x: u8,
    y: u8,
    z: u8,
}

impl Point {
    fn new(x: u8, y: u8, z: u8) -> Point {
        Point { x, y, z }
    }
}

fn p(x: u8, y: u8, z: u8) -> Point {
    Point::new(x, y, z)
}

type PolycubeElemType = u8;
const POLYCUBE_ELEM_BITS: usize = PolycubeElemType::BITS as usize;
#[derive(Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
struct Polycube {
    dims: Point,
    data: Box<[PolycubeElemType]>,
}

impl Polycube {
    fn new_empty(dims: Point) -> Polycube {
        let data_len = ((dims.x as usize * dims.y as usize * dims.z as usize) + POLYCUBE_ELEM_BITS
            - 1)
            / POLYCUBE_ELEM_BITS;
        let data = vec![0; data_len].into_boxed_slice();
        Polycube { dims, data }
    }

    fn new_with_data(dims: Point, data: Box<[PolycubeElemType]>) -> Polycube {
        Polycube { dims, data }
    }

    fn get(&self, coord: Point) -> bool {
        let index = self.coord_to_index(coord);

        (self.data[index / POLYCUBE_ELEM_BITS] & (1 << (index % POLYCUBE_ELEM_BITS))) != 0
    }

    fn set(&mut self, coord: Point, value: bool) {
        let index = self.coord_to_index(coord);
        if value {
            self.data[index / POLYCUBE_ELEM_BITS] |= 1 << (index % POLYCUBE_ELEM_BITS);
        } else {
            self.data[index / POLYCUBE_ELEM_BITS] &= !(1 << (index % POLYCUBE_ELEM_BITS));
        }
    }

    /// Return a new polycube with an empty plane at each edges
    fn expand(&self) -> Polycube {
        let dims = self.dims;
        let new_dims = p(dims.x + 2, dims.y + 2, dims.z + 2);
        let mut new_pc = Polycube::new_empty(new_dims);

        for x in 0..dims.x {
            for y in 0..dims.y {
                for z in 0..dims.z {
                    new_pc.set(p(x + 1, y + 1, z + 1), self.get(p(x, y, z)))
                }
            }
        }
        new_pc
    }

    /// Return a new polycube with empty planes at the edges removed
    fn crop(&self) -> Polycube {
        let dims = self.dims;
        let x_0_empty = self.is_plane_empty(Axis::X, 0);
        let x_last_empty = self.is_plane_empty(Axis::X, dims.x - 1);
        let y_0_empty = self.is_plane_empty(Axis::Y, 0);
        let y_last_empty = self.is_plane_empty(Axis::Y, dims.y - 1);
        let z_0_empty = self.is_plane_empty(Axis::Z, 0);
        let z_last_empty = self.is_plane_empty(Axis::Z, dims.z - 1);

        let mut new_dims = dims;

        if x_0_empty {
            new_dims.x -= 1;
        }
        if x_last_empty {
            new_dims.x -= 1;
        }
        if y_0_empty {
            new_dims.y -= 1;
        }
        if y_last_empty {
            new_dims.y -= 1;
        }
        if z_0_empty {
            new_dims.z -= 1;
        }
        if z_last_empty {
            new_dims.z -= 1;
        }

        let delta_x = if x_0_empty { 1 } else { 0 };
        let delta_y = if y_0_empty { 1 } else { 0 };
        let delta_z = if z_0_empty { 1 } else { 0 };
        let mut new_pc = Polycube::new_empty(new_dims);

        for x in 0..new_dims.x {
            for y in 0..new_dims.y {
                for z in 0..new_dims.z {
                    new_pc.set(
                        p(x, y, z),
                        self.get(p(x + delta_x, y + delta_y, z + delta_z)),
                    )
                }
            }
        }
        new_pc
    }

    fn is_plane_empty(&self, central_axis: Axis, depth: u8) -> bool {
        let dims = self.dims;
        let (dim_a, dim_b) = match central_axis {
            Axis::X => (dims.y, dims.z),
            Axis::Y => (dims.x, dims.z),
            Axis::Z => (dims.x, dims.y),
        };

        for a in 0..dim_a {
            for b in 0..dim_b {
                if self.get_around_axis(central_axis, depth, a, b) {
                    return false;
                }
            }
        }

        true
    }

    fn get_around_axis(&self, axis: Axis, depth: u8, a: u8, b: u8) -> bool {
        let (x, y, z) = match axis {
            Axis::X => (depth, a, b),
            Axis::Y => (a, depth, b),
            Axis::Z => (a, b, depth),
        };
        self.get(p(x, y, z))
    }

    fn coord_to_index(&self, coord: Point) -> usize {
        let dims = self.dims;
        debug_assert!(coord.x < dims.x);
        debug_assert!(coord.y < dims.y);
        debug_assert!(coord.z < dims.z);
        coord.x as usize
            + coord.y as usize * dims.x as usize
            + coord.z as usize * dims.x as usize * dims.y as usize
    }

    fn rot90_x(&self) -> Polycube {
        let dims = self.dims;
        let new_dims = p(dims.x, dims.z, dims.y);
        let mut rotated = Polycube::new_empty(new_dims);

        for x in 0..new_dims.x {
            for y in 0..new_dims.y {
                for z in 0..new_dims.z {
                    rotated.set(p(x, y, z), self.get(p(x, z, dims.z - 1 - y)))
                }
            }
        }
        rotated
    }

    fn rot90_y(&self) -> Polycube {
        let dims = self.dims;
        let new_dims = p(dims.z, dims.y, dims.x);
        let mut rotated = Polycube::new_empty(new_dims);

        for x in 0..new_dims.x {
            for y in 0..new_dims.y {
                for z in 0..new_dims.z {
                    rotated.set(p(x, y, z), self.get(p(z, y, dims.z - 1 - x)))
                }
            }
        }
        rotated
    }

    fn rot90_z(&self) -> Polycube {
        let dims = self.dims;
        let new_dims = p(dims.y, dims.x, dims.z);
        let mut rotated = Polycube::new_empty(new_dims);

        for x in 0..new_dims.x {
            for y in 0..new_dims.y {
                for z in 0..new_dims.z {
                    rotated.set(p(x, y, z), self.get(p(y, dims.y - 1 - x, z)))
                }
            }
        }
        rotated
    }

    fn rot90(&self, axis: Axis) -> Polycube {
        match axis {
            Axis::X => self.rot90_x(),
            Axis::Y => self.rot90_y(),
            Axis::Z => self.rot90_z(),
        }
    }

    fn rotations_around_axis(&self, axis: Axis) -> [Polycube; 4] {
        let a = self.clone();
        let b = a.rot90(axis);
        let c = b.rot90(axis);
        let d = c.rot90(axis);
        [a, b, c, d]
    }

    fn all_rotations(&self) -> Vec<Polycube> {
        let mut result = vec![];
        result.extend_from_slice(&self.rotations_around_axis(Axis::X));

        let r90_y = &self.rot90_y();
        let r180_y = &r90_y.rot90_y();
        let r270_y = &r180_y.rot90_y();
        result.extend_from_slice(&r90_y.rotations_around_axis(Axis::Z));
        result.extend_from_slice(&r180_y.rotations_around_axis(Axis::X));
        result.extend_from_slice(&r270_y.rotations_around_axis(Axis::Z));

        let r90_z = &self.rot90_z();
        let r270_z = &r90_z.rot90_z().rot90_z();
        result.extend_from_slice(&r90_z.rotations_around_axis(Axis::Y));
        result.extend_from_slice(&r270_z.rotations_around_axis(Axis::Y));
        result
    }
}

impl fmt::Display for Polycube {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let dims = self.dims;

        writeln!(f, "x: {}, y: {}, z: {}", dims.x, dims.y, dims.z)?;

        for z in 0..dims.z {
            for y in 0..dims.y {
                for x in 0..dims.x {
                    let value = if self.get(p(x, y, z)) { 1 } else { 0 };
                    write!(f, "{},", value)?;
                }
                writeln!(f)?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}
#[derive(Debug, Serialize, Deserialize)]
struct PolycubeSet {
    map: BTreeMap<Point, BTreeSet<Box<[u8]>>>,
}

impl PolycubeSet {
    fn new() -> PolycubeSet {
        PolycubeSet {
            map: BTreeMap::new(),
        }
    }

    fn contains(&self, value: &Polycube) -> bool {
        if let Some(datas) = self.map.get(&value.dims) {
            datas.contains(&value.data)
        } else {
            false
        }
    }

    fn insert(&mut self, value: Polycube) {
        let datas = self.map.entry(value.dims).or_insert_with(BTreeSet::new);
        datas.insert(value.data);
    }

    fn len(&self) -> usize {
        self.map.values().map(|v| v.len()).sum()
    }

    fn into_iter(self) -> impl Iterator<Item = Polycube> {
        self.map.into_iter().flat_map(|(dims, datas)| {
            datas
                .into_iter()
                .map(move |data| Polycube::new_with_data(dims, data))
        })
    }
}

struct Cache {
    prefix: String,
    bincode_config: bincode::config::Configuration,
}

impl Cache {
    fn filename(&self, n: usize) -> String {
        format!("{}{}.bincode", self.prefix, n)
    }

    fn get(&self, n: usize) -> Option<PolycubeSet> {
        if let Ok(file) = OpenOptions::new().read(true).open(self.filename(n)) {
            let mut file = BufReader::new(file);
            let pcs = bincode::serde::decode_from_std_read(&mut file, self.bincode_config).unwrap();
            Some(pcs)
        } else {
            None
        }
    }

    fn set(&mut self, n: usize, polycubes: &PolycubeSet) {
        let mut file = BufWriter::new(
            OpenOptions::new()
                .write(true)
                .create(true)
                .open(self.filename(n))
                .unwrap(),
        );
        bincode::serde::encode_into_std_write(polycubes, &mut file, self.bincode_config).unwrap();
    }
}

fn find_polycubes(n: usize, cache: &mut Cache) -> PolycubeSet {
    if n == 1 {
        let mut pc = Polycube::new_empty(p(1, 1, 1));
        pc.set(p(0, 0, 0), true);
        let mut set = PolycubeSet::new();
        set.insert(pc);
        return set;
    }

    if let Some(polycubes) = cache.get(n) {
        return polycubes;
    }

    let style = ProgressStyle::with_template(&format!(
        "n={} [{{elapsed_precise}}] {{bar:40.cyan/blue}} {{pos}}/{{len}}, {{eta}} remaining",
        n
    ))
    .unwrap();

    let polycubes_set = RwLock::new(PolycubeSet::new());
    let previous_polycubes = find_polycubes(n - 1, cache);
    let previous_len = previous_polycubes.len() as u64;

    previous_polycubes
        .into_iter()
        .par_bridge()
        .into_par_iter()
        .progress_count(previous_len)
        .with_style(style)
        .for_each(|pc| {
            let mut expanded = pc.expand();
            for x in 1..expanded.dims.x {
                for y in 1..expanded.dims.y {
                    for z in 1..expanded.dims.z {
                        if expanded.get(p(x, y, z)) {
                            for (delta_x, delta_y, delta_z) in [
                                (-1, 0, 0),
                                (0, -1, 0),
                                (0, 0, -1),
                                (1, 0, 0),
                                (0, 1, 0),
                                (0, 0, 1),
                            ] {
                                let new_p = p(
                                    (x as i8 + delta_x) as u8,
                                    (y as i8 + delta_y) as u8,
                                    (z as i8 + delta_z) as u8,
                                );
                                if !expanded.get(new_p) {
                                    expanded.set(new_p, true);
                                    let cropped = expanded.crop();
                                    expanded.set(new_p, false);
                                    let cropped_min =
                                        cropped.all_rotations().into_iter().min().unwrap();

                                    if !polycubes_set.read().unwrap().contains(&cropped_min) {
                                        polycubes_set.write().unwrap().insert(cropped_min);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

    let polycubes_set = polycubes_set.into_inner().unwrap();
    cache.set(n, &polycubes_set);
    polycubes_set
}

fn main() {
    let args = Args::parse();
    let mut cache = Cache {
        prefix: args.cache_prefix,
        bincode_config: bincode::config::standard(),
    };
    let pcs = find_polycubes(args.n, &mut cache);
    println!("len: {}", pcs.len());
}
